import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';

const Adding = ({updateUserList}) => {

    const [data, setData] = useState({
        nom: '',
        prenom: '',
        email: '',
        sexe: ''
        });

        const [listData, setListData] = useState([]);
        const [idCount, setIdCount] = useState(0);
    
        const handleChange = (e) => {
            setData({
            ...data,
            [e.target.name]: e.target.value
            });
        };

        const handleSubmit = (e) => {
            e.preventDefault();
            const newElement = { ...data, id: idCount };
            setListData([...listData, newElement]);
            setData({
                nom: '',
                prenom: '',
                email: '',
                sexe: ''
            });
            updateUserList (data);

            setIdCount(idCount + 1);
            };

    return (
        <div>
            <section className='main'>
                <div className="col-lg-5 col-md-10 col-sm-12">
                    <div className="form-box px-5 py-4">
                        <form onSubmit={handleSubmit}>
                            <h2 className='text-center mb-4'>Enregistrez-vous !</h2>
                            <input type="text" value={data.nom} onChange={handleChange} name="nom" placeholder='Nom' className='form-control mb-3' />
                            <input type="text" value={data.prenom} onChange={handleChange} name="prenom" id="" placeholder='Prenom' className='form-control mb-3'/>
                            <input type="email" value={data.email} onChange={handleChange} name="email" id="" placeholder='Email' className='form-control mb-3'/>
                            <select value={data.sexe} onChange={handleChange} name="sexe" className='form-control mb-3'>
                                <option value="">Sexe</option>
                                <option value="Male">Homme</option>
                                <option value="Female">Femme</option>
                            </select>
                            <Button variant='success' type='submit' className='register-btn mb-3'><b>Validez</b></Button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Adding;
