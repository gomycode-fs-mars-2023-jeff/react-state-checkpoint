import React from 'react';
import Table from 'react-bootstrap/Table';


const AddingList = ({listData}) => {
    return (
        <div>
            <div className="col-lg-7 col-md-12 d-flex align-items-center">
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Email</th>
                                <th>Sexe</th>
                            </tr>
                        </thead>
                        <tbody>
                            {listData && listData.map((item, index) => (
                                <tr key={item}>
                                    <td>{index + 1}</td>
                                    <td>{item.nom}</td>
                                    <td>{item.prenom}</td>
                                    <td>{item.email}</td>
                                    <td>{item.sexe}</td>
                                </tr>
                            ))}                 
                        </tbody>
                    </Table>
            </div>
        </div>
    );
}

export default AddingList;
