import { useState } from 'react';
import './App.css';
import './form/Adding.css';
import Adding from "./form/Adding"
import AddingList from './form/AddingList';

function App() {

  const [listData, setListData] = useState([]);
  function updateUserList (user) {
    setListData([...listData, user])
  }

  return (
    <div className="App">
        <Adding updateUserList = {updateUserList}></Adding>
        <AddingList listData = {listData}></AddingList>
    </div>
  );
}

export default App;
